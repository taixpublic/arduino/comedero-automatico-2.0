#include "ModuloWiFi.h"

ModuloWiFi::ModuloWiFi() {
    ConfigApp* cfgApp = ConfigApp::getInstance();
    ssid = cfgApp->nameWiFi();
    swWiFi = cfgApp->hasWiFi();
    passWifi = cfgApp->passWiFi();
}

void ModuloWiFi::setup(int nRetriesAux) {
    if (!swWiFi)    return;
    delay(100);
    WiFi.begin(ssid.c_str(), passWifi.c_str());
    while (WiFi.status() != WL_CONNECTED && nRetriesAux >= 0) {
        nRetriesAux--;
        delay(500);
    }

    if (WiFi.status() != WL_CONNECTED)
        Serial.println("No estoy conectado a la red WiFi");
}
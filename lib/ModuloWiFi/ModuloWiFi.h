#ifndef ModuloWiFi_H
#define ModuloWiFi_H

#include <ConfigApp.h>
#include <Arduino.h>
#include <WiFi.h>

class ModuloWiFi {

    private:
        bool swWiFi;
        String ssid;
        String passWifi;

    public:
        ModuloWiFi();
        void setup(int nRetriesAux);
};

#endif
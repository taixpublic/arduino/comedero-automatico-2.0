#ifndef ModuloTelegram_h
#define ModuloTelegram_h

#include <UniversalTelegramBot.h>
#include <WiFiClientSecure.h>
#include <ConfigApp.h>
#include <Arduino.h>
#include <WiFi.h>

WiFiClientSecure client;
UniversalTelegramBot bot = UniversalTelegramBot("", client);

class ModuloTelegram {

    private:
        String token;
        String chatId;
        bool swTelegram;

    public:
        ModuloTelegram();
        void setup();

        void avisoComidaDada();
};

#endif
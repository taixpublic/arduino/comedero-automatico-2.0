#include "ModuloTelegram.h"

ModuloTelegram::ModuloTelegram() {
    ConfigApp* cfgApp = ConfigApp::getInstance();
    chatId = cfgApp->chatIdTelegram();
    token = cfgApp->tokenBotTelegram();
    swTelegram = cfgApp->hasTelegram();
    swTelegram &= cfgApp->isOnline();
    bot.updateToken(token);
}

void ModuloTelegram::setup() {
    if (!swTelegram)    return;
    client.setCACert(TELEGRAM_CERTIFICATE_ROOT);
}

void ModuloTelegram::avisoComidaDada() {
    if (!swTelegram)    return;
    bot.sendMessage(chatId, "Me acaban de dar de comer");
}
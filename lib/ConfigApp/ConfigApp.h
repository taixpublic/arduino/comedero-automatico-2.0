#ifndef ConfigApp_H
#define ConfigApp_H

#include <Preferences.h>
#include <Arduino.h>
#include <WiFi.h>

class ConfigApp {

  private:
    ConfigApp();
    Preferences preferences;
    static ConfigApp* instance;

    bool swData;
    int durDosis;
    long durEntreDosis;
    long durSleepExterno;

    bool swWiFi;
    String ssid;
    String passwifi;

    bool swMQTT;
    String servermqtt;
    int puertomqtt;
    String idmqtt;
    String usermqtt;
    String passmqtt;
    String topicmqtt;
    
    bool swTelegram;
    String token;
    String idchat;

  public:
    ConfigApp(ConfigApp &other) = delete;
    void operator=(const ConfigApp &) = delete;

    static ConfigApp* getInstance();

    boolean hasData();

    int duracionDosis();
    int sleepEntreDosis();
    int sleepExterno();

    boolean hasWiFi();
    boolean isOnline();
    String nameWiFi();
    String passWiFi();

    boolean hasMQTT();
    String ipServidorMQTT();
    int puertoServidorMQTT();
    String idClienteMQTT();
    String usuarioMQTT();
    String passMQTT();
    String topicMQTT();

    boolean hasTelegram();
    String tokenBotTelegram();
    String chatIdTelegram();
};

#endif
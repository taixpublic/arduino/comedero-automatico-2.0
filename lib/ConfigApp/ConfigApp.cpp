#include "ConfigApp.h"

ConfigApp* ConfigApp::instance = nullptr;

ConfigApp* ConfigApp::getInstance() {
    if (instance == nullptr)
        instance = new ConfigApp();
    return instance;
}

ConfigApp::ConfigApp() {
  preferences.begin("ComeAut2.0", false);
  durDosis = preferences.getInt("durDosis", 0);
  durEntreDosis = preferences.getInt("dEnDosis", 0);
  durSleepExterno = preferences.getInt("sleepExt", 0);

  ssid = preferences.getString("ssid", "");
  swWiFi = preferences.getBool("swWiFi", false);
  passwifi = preferences.getString("passWiFi", "");

  swMQTT = preferences.getBool("swMQTT", false);
  servermqtt = preferences.getString("ipMQTT", "");
  puertomqtt = preferences.getInt("puMQTT", 0);
  idmqtt = preferences.getString("idMQTT", "");
  usermqtt = preferences.getString("userMQTT", "");
  passmqtt = preferences.getString("passMQTT", "");
  topicmqtt = preferences.getString("topicMQTT", "");

  swTelegram = preferences.getBool("swTelegra", false);
  token = preferences.getString("tokenTele", "");
  idchat = preferences.getString("idChatTel", "");
  preferences.end();

  swData = durDosis > 0;
}

bool ConfigApp::hasData() {
  return swData;
}

int ConfigApp::duracionDosis() {
  return durDosis;
}

int ConfigApp::sleepEntreDosis() {
  return durEntreDosis;
}

int ConfigApp::sleepExterno() {
  return durSleepExterno;
}

boolean ConfigApp::isOnline() {
  return WiFi.isConnected();
}

boolean ConfigApp::hasWiFi() {
  return swWiFi;
}

String ConfigApp::nameWiFi() {
  return ssid;
}

String ConfigApp::passWiFi() {
  return passwifi;
}

boolean ConfigApp::hasMQTT() {
  return swMQTT;
}

String ConfigApp::ipServidorMQTT() {
  return servermqtt;
}

int ConfigApp::puertoServidorMQTT() {
  return puertomqtt;
}

String ConfigApp::idClienteMQTT() {
  return idmqtt;
}

String ConfigApp::usuarioMQTT() {
  return usermqtt;
}

String ConfigApp::passMQTT() {
  return passmqtt;
}

String ConfigApp::topicMQTT() {
  return topicmqtt;
}

boolean ConfigApp::hasTelegram() {
  return swTelegram;
}

String ConfigApp::tokenBotTelegram() {
  return token;
}

String ConfigApp::chatIdTelegram() {
  return idchat;
}
#include "LogicApp.h"

LogicApp::LogicApp() {
    cfgApp = ConfigApp::getInstance();
}

void LogicApp::darComida(int pinMotor) {
    digitalWrite(pinMotor, HIGH);
    delay(cfgApp->duracionDosis() * 1000);
    digitalWrite(pinMotor, LOW);
}

void LogicApp::dormir() {
    esp_sleep_enable_timer_wakeup(cfgApp->sleepEntreDosis() * uS_TO_S_FACTOR);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_SLOW_MEM, ESP_PD_OPTION_OFF);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_FAST_MEM, ESP_PD_OPTION_OFF);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
    Serial.println("Procedo a dormir hasta la siguiente dosis");
    delay(10);
    esp_deep_sleep_start();
}
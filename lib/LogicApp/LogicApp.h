#ifndef LogicApp_H
#define LogicApp_H

#include <ConfigApp.h>
#include <Arduino.h>

/* Conversion factor for micro seconds to seconds */
#define uS_TO_S_FACTOR 1000000  

class LogicApp {

  private:
    ConfigApp* cfgApp = nullptr;

  public:
    LogicApp();

    void darComida(int pinMotor);
    void dormir();
};

#endif
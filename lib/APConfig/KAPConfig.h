// Punto de acceso
const char* ssid     = "Comedero automatico 2.0";
const char* password = "123456789";

// Paginas Web
const char * indexAP = R"HTML(
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />        
        <title>CFG. comedero</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <style>
            body{
                margin: 0px;
                padding: 0px;
                padding-top: 100px;
                padding-bottom: 50px;
            }
            header {
                background-color: #000;
                text-align: center;
                position: fixed;
                height: 100px;
                color: #fff;
                width: 100%;
                z-index: 1;
                top: 0;
            }
            .section {
                border: 1px solid grey;
                border-radius: 16px;
                position: relative;
                text-align: center;
                padding: 15px;
                margin: 10px auto 0;
                width: 90%;
            }
            div[disabled] {
                pointer-events: none;
                opacity: 0.1;
            }
            .section > .info {
                position: absolute;
                font-style: italic;
                text-align: right;
                padding: 5px;
                max-width: 70%;
                right: 0;
                top: 0;
            }
            .submit {
                position: relative;
                text-align: center;
                margin: 10px auto;
                display:block;
                padding: 5px;
            }
            footer {
                background-color: #000;
                text-align: center;
                line-height: 50px;
                position: fixed;
                color: #fff;
                height: 50px;
                width: 100%;
                z-index: 1;
                bottom: 0;
            }

            .switch {
                position: absolute;
                display: inline-block;
                width: 60px;
                height: 34px;
                left: 15px;
            }
            .switch input { 
                opacity: 0;
                width: 0;
                height: 0;
            }
            .slider {
                position: absolute;
                cursor: pointer;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: #ccc;
                -webkit-transition: .4s;
                transition: .4s;
            }
            .slider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }
            input:checked + .slider {
                background-color: #000000;
            }
            input:focus + .slider {
                box-shadow: 0 0 1px #000000;
            }
            input:checked + .slider:before {
                -webkit-transform: translateX(26px);
                -ms-transform: translateX(26px);
                transform: translateX(26px);
            }
        </style>
    </head>
    <body>
        <header>
            <h1>Configuraci&oacute;n comedero autom&aacute;tico</h1>
        </header>
        <form action="/save" method="GET">
            <div class="section">
                <div class="info">Nota: las unidades de medida van en segundos</div>
                <h1>Cfg. general</h1>
                <div class="data">
                    <label for="durDosis">
                        Duraci&oacute;n de las dosis
                        <input type="number" name="durDosis" min="1" max="60" />
                    </label><br />
                    <label for="dEnDosis">
                        Duraci&oacute;n entre dosis
                        <input type="number" name="dEnDosis" min="180" max="86400" />
                    </label><br />
                    <label for="sleepExt">
                        Temporizador externo
                        <input type="number" name="sleepExt" min="0" max="86400" value="0" />
                    </label>
                </div>
            </div>

            <div class="section">
                <label class="switch">
                  <input type="checkbox" name="swWiFi" onChange="toggleSection(this)" id="wifi" />
                  <span class="slider"></span>
                </label>
                <h1>Cfg. WiFi</h1>
                <div disabled class="data" id="section_wifi">
                    <label for="ssid">
                        SSID
                        <input type="text" name="ssid" />
                    </label><br />
                    <label for="passWiFi">
                        Contrase&ntilde;a
                        <input type="password" name="passWiFi" />
                    </label>
                </div>
            </div>

            <div class="section">
                <label class="switch">
                  <input type="checkbox" name="swMQTT" onChange="toggleSection(this)" id="mqtt" />
                  <span class="slider"></span>
                </label>
                <div class="info">Nota: este m&oacute;dulo funciona con la integraci&oacute;n WiFi</div>
                <h1>Cfg. MQTT</h1>
                <div disabled class="data" id="section_mqtt">
                    <label for="ipMQTT">
                        IP del servidor
                        <input type="text" name="ipMQTT" />
                    </label><br />
                    <label for="puMQTT">
                        Puerto del servidor
                        <input type="number" name="puMQTT" value="1883" min="1" />
                    </label><br />
                    <label for="idMQTT">
                        Identificador del cliente
                        <input type="text" name="idMQTT" />
                    </label><br />
                    <label for="userMQTT">
                        Usuario
                        <input type="text" name="userMQTT" />
                    </label><br />
                    <label for="passMQTT">
                        Contrase&ntilde;a
                        <input type="password" name="passMQTT" />
                    </label><br />
                    <label for="topicMQTT">
                        Topico del comedero
                        <input type="text" name="topicMQTT" />
                    </label>
                </div>
            </div>

            <div class="section">
                <label class="switch">
                  <input type="checkbox" name="swTelegra" onChange="toggleSection(this)" id="telegram" />
                  <span class="slider"></span>
                </label>
                <div class="info">Nota: este m&oacute;dulo funciona con la integraci&oacute;n WiFi</div>
                <h1>Cfg. Telegram</h1>
                <div disabled class="data" id="section_telegram">
                    <label for="tokenTele">
                        Token
                        <input type="text" name="tokenTele" />
                    </label><br />
                    <label for="idChatTel">
                        Identificador del chat
                        <input type="text" name="idChatTel" />
                    </label>
                </div>
            </div>
            <input type="submit" class="submit" value="Submit" />
        </form>
        <footer>
            2021 | v. 2021.05.0-alpha | @TaixMiguel
        </footer>
        <script>
            function toggleSection(element) {
              var section = document.getElementById('section_' + element.id);
              if (element.checked)  section.removeAttribute('disabled');
              else  section.setAttribute('disabled', '');
            }
          </script>
    </body>
</html>
)HTML";

const char * saveAP = R"HTML(
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />        
        <title>CFG. comedero</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <style>
            body{
                margin: 0px;
                padding: 0px;
                padding-top: 100px;
                padding-bottom: 50px;
            }
            header {
                background-color: #000;
                text-align: center;
                position: fixed;
                height: 100px;
                color: #fff;
                width: 100%;
                z-index: 1;
                top: 0;
            }
            body > p {
                position: relative;
                text-align: center;
            }
            footer {
                background-color: #000;
                text-align: center;
                line-height: 50px;
                position: fixed;
                color: #fff;
                height: 50px;
                width: 100%;
                z-index: 1;
                bottom: 0;
            }
        </style>
    </head>
    <body>
        <header>
            <h1>Configuraci&oacute;n comedero autom&aacute;tico</h1>
        </header>
        <p>
            La configuraci&oacute;n ha sido guardada.<br />
            Se reiniciar&aacute; el dispositivo en 5 segundos.
        </p>
        <footer>
            2021 | v. &VERSION | @TaixMiguel
        </footer>
    </body>
</html>
)HTML";
#ifndef AccessPoint_h
#define AccessPoint_h

#include <Preferences.h>
#include <KAPConfig.h>
#include <Arduino.h>
#include <WiFi.h>

WiFiServer server(80);

class AccessPoint {

    private:
        const char* nApp;
        const char* vApp;
        Preferences preferences;

        String getHtmlSave();
        String getHtmlIndex();
        void savePreferences(String params);
        String getValueParam(String params, String param);
        void saveInt(Preferences preferences, String params, String param);
        void saveLong(Preferences preferences, String params, String param);
        void saveString(Preferences preferences, String params, String param);
        void saveBoolean(Preferences preferences, String params, String param);

    public:
        AccessPoint(char* nApp, char* vApp);
        void setup();
        void loadAccessPoint();
};

#endif
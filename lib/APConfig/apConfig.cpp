#include "apConfig.h"

AccessPoint::AccessPoint(char* nApp, char* vApp) {
    this->nApp = nApp;
    this->vApp = vApp;
}

void AccessPoint::setup() {
    Serial.print("Configurando punto de acceso… ");
    WiFi.softAP(ssid, password);

    IPAddress IP = WiFi.softAPIP();
    Serial.print("\nDirección IP del punto de acceso: ");
    Serial.println(IP);
    server.begin();
}

void AccessPoint::loadAccessPoint() {
    WiFiClient client = server.available();
    String cabecera = "";
    String header = "";
    if (client) {
        String currentLine = "";
        while (client.connected()) {
            if (client.available()) {
                char c = client.read();
                if (cabecera.isEmpty() && c == '\r') cabecera = header;
                header += c;

                if (c == '\n') {
                    if (currentLine.length() == 0) {
                        client.println("HTTP/1.1 200 OK");
                        client.println("Content-type:text/html");
                        client.println("Connection: close");
                        client.println();

                        if (header.indexOf("GET /save") >= 0) {
                            cabecera = cabecera.substring(cabecera.indexOf("?")+1, cabecera.indexOf(" ", 10));
                            savePreferences(cabecera);
                            client.println(getHtmlSave());
                            client.stop();
                            delay(5000);
                            ESP.restart();
                        } else            client.println(getHtmlIndex());
                        client.println();
                        break;
                    } else              currentLine = "";
                } else if (c != '\r')   currentLine += c;
            }
        }
        client.stop();
    }
}

void AccessPoint::savePreferences(String params) {
    // Cfg. general
    saveInt(preferences, params, "durDosis");
    saveInt(preferences, params, "dEnDosis");
    saveInt(preferences, params, "sleepExt");

    // Cfg. WiFi
    saveBoolean(preferences, params, "swWiFi");
    saveString(preferences, params,  "ssid");
    saveString(preferences, params,  "passWiFi");

    // Cfg. MQTT
    saveBoolean(preferences, params, "swMQTT");
    saveString(preferences, params,  "ipMQTT");
    saveLong(preferences, params,    "puMQTT");
    saveString(preferences, params,  "idMQTT");
    saveString(preferences, params,  "userMQTT");
    saveString(preferences, params,  "passMQTT");
    saveString(preferences, params,  "topicMQTT");

    // Cfg. Telegram
    saveBoolean(preferences, params, "swTelegra");
    saveString(preferences, params,  "tokenTele");
    saveString(preferences, params,  "idChatTel");
}

String AccessPoint::getValueParam(String params, String param) {
    int index0 = params.indexOf(param + "=");
    index0 = params.indexOf("=", index0) + 1;
    int index1 = params.indexOf("&", index0);
    if (index1 == -1)   index1 = params.length();
    return params.substring(index0, index1);
}

void AccessPoint::saveLong(Preferences preferences, String params, String param) {
    String value = getValueParam(params, param);
    long val = atoi(value.c_str());
    preferences.begin(nApp, false);
    preferences.putLong(param.c_str(), val);
    preferences.end();
}

void AccessPoint::saveInt(Preferences preferences, String params, String param) {
    String value = getValueParam(params, param);
    int val = atoi(value.c_str());
    preferences.begin(nApp, false);
    preferences.putInt(param.c_str(), val);
    preferences.end();
}

void AccessPoint::saveBoolean(Preferences preferences, String params, String param) {
    String value = getValueParam(params, param);
    bool swVal = value.equalsIgnoreCase("on");
    preferences.begin(nApp, false);
    preferences.putBool(param.c_str(), swVal);
    preferences.end();
}

void AccessPoint::saveString(Preferences preferences, String params, String param) {
    String value = getValueParam(params, param);
    preferences.begin(nApp, false);
    preferences.putString(param.c_str(), value);
    preferences.end();
}

String AccessPoint::getHtmlSave() {
    String html(saveAP);
    html.replace("&VERSION", vApp);
    return html;
}

String AccessPoint::getHtmlIndex() {
    String html(indexAP);
    html.replace("&VERSION", vApp);
    return html;
}
#include <Arduino.h>

#include "ModuloTelegram.cpp"
#include "ModuloWiFi.cpp"
#include "ConfigApp.cpp"
#include "apConfig.cpp"
#include "LogicApp.cpp"
#include "constants.h"
#include <Arduino.h>

void setup() {
  Serial.begin(115200);
  ConfigApp* cfgApp = ConfigApp::getInstance();
  if (!cfgApp->hasData()) {
    AccessPoint(nApp, vApp).setup();
    return;
  }

  ModuloWiFi mWiFi = ModuloWiFi();
  mWiFi.setup(nRetries);

  ModuloTelegram mTelegram = ModuloTelegram();
  mTelegram.setup();
  pinMode(pinMotor, OUTPUT);
}

void loop() {
  ConfigApp* cfgApp = ConfigApp::getInstance();
  if (!cfgApp->hasData()) {
    AccessPoint(nApp, vApp).loadAccessPoint();
    return;
  }

  LogicApp logicApp = LogicApp();
  logicApp.darComida(pinMotor);
  ModuloTelegram mTelegram = ModuloTelegram();
  mTelegram.avisoComidaDada();
  logicApp.dormir();
}
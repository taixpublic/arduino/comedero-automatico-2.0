# Comedero automático 2.0

## Configuración inicial del comedero automático
El comedero automático, cuando no tiene la configuración establecida, funciona como un punto de acceso (a partir de ahora le llamaremos PA, un PA se puede ver como un router WiFi) al que deberemos conectar para introducir nuestra configuración.

El punto WiFi tendrá de SSID (nombre) `Comedero automatico 2.0` y su contraseña es `123456789`, (esto es así para que ningún vecino pueda configurar nuestro comedero antes de que lo hagamos nosotros. Por otro lado, una vez que lo tengamos configurado y se haya reiniciado, cosa que hace automáticamente, no volverá a crear el punto de acceso salvo que eliminemos la configuración).

Una vez que estemos conectados al PA, entramos en la página 192.168.4.1 (o la que te salga en la terminal del ESP32) y rellenamos los diferentes puntos de configuración disponibles.

### Cfg. general
* **Duración de las dosis:** establece el tiempo (en segundos) que permanecerá el motor encendido dispensando comida.
* **Duración entre dosis:** establece el tiempo total (en segundos) en el que el dispositivo permanecerá en modo ahorro antes de la siguiente dosis
* **Temporizador externo:** en caso de tener una placa temporizador de bajo consumo, indicar (en segundos) el tiempo configurado de su modo ahorro (se restará del tiempo total parametrizado en el campo "Duración entre dosis").

### Cfg. WiFi
* **SSID:** establecer el SSID (nombre) de nuestro WiFi domestico.
* **Contraseña:** establecer la contraseña de nuestro WiFi domestico.

### Cfg. MQTT
* **IP del servidor:** dirección IP del servidor MQTT
* **Puerto del servidor:** puerto que utiliza el broker MQTT.
* **Identificador del cliente:** identificador del cliente MQTT que utilizará el dispositivo.
* **Usuario:** usuario MQTT que utilizará el dispositivo.
* **Contraseña:** contraseña asociada al usuario MQTT que utilizará el dispositivo.
* **Topico del comedero:** topic que se utilizará para interactuar con el dispositivo.

### Cfg. Telegram
* **Token:** token del bot que utilizará el comedero para interactuar con el usuario.
* **Identificador del chat:** identificador del chat dónde se interactuará con el usuario.

## Módulo Telegram
Si activamos el módulo Telegram, recibiremos el mensaje _"Me acaban de dar de comer"_ en el chat indicado cada vez que el comedero dé de comer a nuestra mascota.

Es imprescindible tener activado el módulo WiFi, sin dicho módulo la integración con Telegram **no** funciona.
